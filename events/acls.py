from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    """
    Get the photo for a location using the Pexels API.
    """
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {"query": f'{city} {state}'}
    url = 'https://api.pexels.com/v1/search/'
    response = requests.get(url, headers=headers, params=payload)
    images = json.loads(response.content)
    picture = images["photos"][0]["src"]["original"]
    return { "picture_url": picture }


def get_weather_data(city, state):
    # Use the Open Weather API
    pass
def get_lat_long(location):
    """
    Get the latitude and longitude of a location using the Open Weather API.
    """
    base_url = 'http://api.openweathermap.org/geo/1.0/direct'
    params = {
        'q': f"{location.city},{location.state.abbreviation}, USA",
        'appid': OPEN_WEATHER_API_KEY
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    return { "latitude" : parsed_json[0]["lat"], "longitude": parsed_json[0]["lon"] }

def get_weather_data(location):
    """
    Get the weather data for a location using the Open Weather API.
    """
    # get lat and long of location
    lat_long = get_lat_long(location)

    # get the weather data from the api
    base_url = "http://api.openweathermap.org/data/2.5/weather"
    params = {
        'lat': lat_long["latitude"],
        'lon': lat_long["longitude"],
        'appid': OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    response = requests.get(base_url, params=params)
    # parse the response object
    parsed_json = json.loads(response.content)
    # create a new dictionary
    # add temp and description properties to new dictionary
    weather_data = {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }

    return weather_data
